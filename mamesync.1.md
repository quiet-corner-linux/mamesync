---
title: mamesync
section: 1
header: User Manual
footer: mamesync 0.1.0
date: July 11, 2023
---

NAME
====

mamesync - synchronize your MAME library

SYNOPSIS
========

Usage
-----

| `mamesync [COMMAND]`
| `mamesync [COMMAND] --help` | `-h`
| `mamesync --version` | `-v`

Commands
--------

| Long      | Short | Help              |
|:--------- |:----- |:----------------- |
| `filters` | `f`   | manage filters    |
| `sync`    | `s`   | synchronize files |


Options
-------

| Long        | Short      | Help                |
|:----------- |:---------- |:------------------- |
| `--help`    | `-h`       | Show this help      |
| `--version` | `-v`       | Show version number |

FILTERS
=======

Usage
-----

| `mamesync filters COMMAND`
| `mamesync filters [COMMAND] --help` | `-h`

Commands
--------

| Long      | Short | Help              |
|:--------- |:----- |:----------------- |
| `destroy` | `d`   | destroy filter(s) |
| `edit`    | `e`   | edit filter       |
| `index`   | `i`   | index of filters  |
| `new`     | `n`   | new filter        |
| `show`    | `s`   | show filter(s)    |

Options
-------

| Long     | Short | Help           |
|:-------- |:----- |:-------------- |
| `--help` | `-h`  | Show this help |

SYNC
====

Usage
-----

| `mamesync sync SOURCE [DESTINATION] [OPTIONS]`
| `mamesync sync --help` | `-h`

Options
-------

| Long        | Short | Repeatable | Help                                               |
|:----------- |:----- |:----------:|:-------------------------------------------------- |
| `--delete`  | `-d`  | [ ]        | delete extraneous files from destination directory |
| `--dry-run` | `-n`  | [ ]        | perform a trial run with no changes made           |
| `--filter`  | `-f`  | [x]        | title of filter(s)                                 |
| `--help`    | `-h`  | [ ]        | Show this help                                     |

Arguments
---------

| Argument      | Help                | Default Value       |
|:------------- |:------------------- |:------------------- |
| `SOURCE`      | path of source      |                     |
| `DESTINATION` | path of destination | ~/shared/roms/mame/ |

Examples
--------

| `mamesync sync "/run/media/keiljr/ARCADE-USB/MAME 0.252 ROMs (non-merged)/"`


ENVIRONMENT
===========

| Variable | Default Value |
|:-------- |:------------- |
| `PREFIX` | /usr/local    |

BUGS
====

Please report bugs at [https://gitlab.com/quiet-corner-linux/mamesync](https://gitlab.com/quiet-corner-linux/mamesync).

EXAMPLES
========

| `mamesync list index`
| `mamesync sync "/run/media/arcade/ARCADE-USB/MAME 0.252 ROMs (non-merged)/" --filter=nofiller_2p`

FILES
=====

The following files are installed locally.

| ${PREFIX}/bin/mamesync
| ${PREFIX}/man/man1/mamesync.1.gz
| ${PREFIX}/share/mamesync/filters/neogeo.tag
| ${PREFIX}/share/mamesync/filters/nofiller_2p.tag
| ${PREFIX}/share/mamesync/filters/nofiller_4p.tag
| ${PREFIX}/share/mamesync/filters/nofiller_chd.tag
| ${PREFIX}/share/mamesync/filters/vsnes.tag

If the following user path does not exist when executing mamesync, the directories will be created and filters will be copied to this path. You may modify them as you wish. To restore the filters to their original state, remove the directory and execute mamesync.

| ~/.mamesync/filters
