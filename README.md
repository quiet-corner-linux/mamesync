# Mamesync - synchronize your MAME library

Do you keep your MAME romset, multimedia, and extras up to date every month on an external disk? Use Mamesync to synchronize the files from your external disk to your arcade machine. You may use filters to syncronize only the best arcade games. 10k+ games are good for preservation, not for game play.

## Usage

Usage documentation may be found a few different ways.

* `man mamesync`
* `mamesync --help`
* `mamesync -h`
* `mamesync [COMMAND] --help`
* `mamesync [COMMAND] -h`

## Installation

Mamesync is intended to work on Linux Operating Systems. It was developed using Arch Linux. Adjust commands for your distro's package manager. Mamesync may work on Mac OS X.

### Requirements

Requirements

* git
* install (coreutils)
* make (base-devel)
* ruby

Installation of packages requires root privilages. Use `sudo` if you are not logged in as root.

Arch Linux

```
sudo pacman -Syu git coreutils base-devel ruby
```

### Clone the project

Clone the project, and change the working directory to the newly cloned project.

```
git clone https://gitlab.com/quiet-corner-linux/mamesync.git && cd mamesync
```

### Build

```
make build
```

### Install

Installation of packages requires root privilages. Use `sudo` if you are not logged in as root.

```
make install
```

Verify your installation.

```
where mamesync
mamesync -v
mamesync -h
man mamesync
```

### Uninstall

Installation of packages requires root privilages. Use `sudo` if you are not logged in as root.

```
make uninstall
```

Verify that mamesync was sucessfully uninstalled. The following commands should fail.

```
where mamesync
man mamesync
```

### Clean

Remove build and vendor (Ruby Bundler gem packages) files.

```
make clean
```

### Clean-build

Remove build files.

```
make clean-build
```
