generate_tags_function() {
  tags=()

  for i in $@; do
    while read -r line; do
      tags+=($line)
    done <${user_data_path}/filters/${i}.tag
  done

  tags=(`echo ${tags[@]} | tr " " "\n" | sort -u`)
}
