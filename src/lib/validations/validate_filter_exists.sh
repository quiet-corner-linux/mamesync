validate_filter_exists() {
	[[ -e ${user_data_path}/filters/$1.tag ]] || echo "must be an existing filter"
}
