name=mamesync
app_data_path="${PREFIX}/share/${name}"
user_data_path="${HOME}/.${name}"

if [[ ! -d user_data_path/filters ]]; then
	install -d ${user_data_path}/filters
	install -m644 ${app_data_path}/filters/* ${user_data_path}/filters/
fi
