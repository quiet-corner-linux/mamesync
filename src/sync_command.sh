egress() {
	rm -f $tempfile
}
trap egress EXIT

eval source_path=${args[source]}
eval destination_path=${args[destination]}
eval filters=(${args[--filter]})
opts=("--archive" "--compress" "--info=progress2")

echo "$(blue_bold ::) $(bold Synchronizing files...)"

if [[ ${args[--dry-run]} ]]; then
	echo "$(yellow_bold :: warning:) This will be a dry run."
	opts+=("--dry-run")
fi

if [[ ${args[--delete]} && ! ${args[--dry-run]} && -d $destination_path ]]; then
	echo "$(yellow_bold :: warning:) You are about to potentially delete extraneous files from the destination directory:"
	echo "  ${destination_path}"
	echo "This could break your system should you have the incorrect path."
  echo "$(bold ::) Proceed with installation? [y/N]"
	read -rp ">> " input
	if [[ $input != "Y" && $input != "y" ]]; then exit 1; fi
	opts+=("--delete" "--delete-excluded")
fi

if [[ ${#filters[@]} -gt 0 ]]; then
	echo "Generating tags..."
	generate_tags_function "${filters[@]}"
	tempfile=$(mktemp --tmpdir mamesync-XXXXX)
	printf '%s.*\n' "${tags[@]}" > ${tempfile}
	opts+=("--include-from=${tempfile}" "--exclude=*")
fi

if [[ ! -d $destination_path ]]; then
	echo "Creating destination path..."
	if [[ ! ${args[--dry-run]} ]]; then mkdir -p $destination_path; fi
fi

echo Starting synchronization...
eval rsync ${opts[@]} $source_path $destination_path
echo "Synchronization is complete."
