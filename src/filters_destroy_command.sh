eval titles=(${args[title]})

echo "$(blue_bold ::) $(bold Destroying filters...)"

for i in ${titles[@]}; do
	echo "Destroying ${i} filter..."
	rm ${user_data_path}/filters/${i}.tag
done

echo "All filters destroyed."
