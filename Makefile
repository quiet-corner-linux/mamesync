SHELL=/bin/sh
PREFIX?=/usr/local

name=mamesync

exec_prefix=$(PREFIX)
bindir=$(exec_prefix)/bin
datarootdir=$(PREFIX)/share
datadir=$(datarootdir)
mandir=$(datarootdir)/man
man1dir=$(mandir)/man1

.PHONY: build install uninstall clean clean-build

build:
	@echo ":: Building $(name)..."
	bundle install
	mkdir -p build
	bundle exec bashly generate --env=production
	PREFIX=$(PREFIX) envsubst < $(name).1.md | pandoc --standalone --from=markdown --to=man | gzip > build/$(name).1.gz
	@echo "Build has finished."

install:
	@echo ":: Installing $(name)..."
	install -CD -m755 build/$(name) $(DESTDIR)$(bindir)/$(name)
	install -CD -m644 build/$(name).1.gz $(DESTDIR)$(man1dir)
	install -Cd $(DESTDIR)$(datadir)/$(name)/filters
	install -CD -m644 filters/* $(DESTDIR)$(datadir)/$(name)/filters/
	@echo "Installation is complete."

uninstall:
	@echo ":: Uninstalling $(name)..."
	rm $(DESTDIR)$(bindir)/$(name)
	rm $(DESTDIR)$(man1dir)/$(name).1.gz
	rm -rf $(DESTDIR)$(datadir)/$(name)
	@echo "Removal is complete."

clean:
	@echo ":: Cleaning $(name) build and vendor files..."
	rm -rf build
	rm -rf vendor
	@echo "Build and vendor files have been removed."

clean-build:
	@echo ":: Cleaning $(name) build files..."
	rm -rf build
	@echo "Build files have been removed."
